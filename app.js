require('stackify-node-apm')
const express = require('express');


const app = express();

var io = require('socket.io-client');

var socket = io.connect('https://kings-socket.zodexchange.com');
var socket1 = io.connect('https://common-socket.zodexchange.com');
var socket2 = io.connect("https://common-rates.zodexchange.com");
var socket3= io.connect("https://common-socket.zodexchange.com");


app.use((req,res,next)=>{

    res.status(200).json({
        id1:socket.id,
        id2:socket1.id,
        id3:socket2.id,
        id4:socket3.id
      
    });
})

module.exports = app;