/**
 * Stackify Node APM Configuration
 */
exports.config = {
  /**
   * Your application name.
   */
  application_name: 'testsocket',
  /**
   * Your environment name.
   */
  environment_name: 'Production'
}
